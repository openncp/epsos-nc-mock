package eu.epsos.protocolterminators.ws.server.xdr.impl;

import eu.epsos.protocolterminators.ws.server.common.NationalConnectorGateway;
import eu.epsos.protocolterminators.ws.server.exception.NIException;
import eu.epsos.protocolterminators.ws.server.xdr.DocumentProcessingException;
import eu.epsos.protocolterminators.ws.server.xdr.DocumentSubmitInterface;
import fi.kela.se.epsos.data.model.ConsentDocumentMetaData;
import fi.kela.se.epsos.data.model.DocumentAssociation;
import fi.kela.se.epsos.data.model.EDDocumentMetaData;
import fi.kela.se.epsos.data.model.EPSOSDocument;
import org.apache.log4j.Logger;
import tr.com.srdc.epsos.securityman.exceptions.InsufficientRightsException;

/**
 * A national implementation for clinical document submission.
 *
 * @author Kostas Karkaletsis, k.karkaletsis@gnomon.com.gr
 */
public class DocumentSubmitImpl extends NationalConnectorGateway implements DocumentSubmitInterface {

    // Logger
    public static Logger logger = Logger.getLogger(DocumentSubmitImpl.class);

    @Override
    public void submitDispensation(EPSOSDocument dispensationDocument) throws NIException, InsufficientRightsException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void cancelDispensation(DocumentAssociation<EDDocumentMetaData> dispensationToDiscard) throws NIException, InsufficientRightsException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void submitPatientConsent(EPSOSDocument consentDocument) throws NIException, InsufficientRightsException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void cancelConsent(DocumentAssociation<ConsentDocumentMetaData> consentToDiscard) throws NIException, InsufficientRightsException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void submitHCER(EPSOSDocument hcerDocument) throws DocumentProcessingException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
