package eu.epsos.protocolterminators.ws.server.xca.impl;

import eu.epsos.protocolterminators.ws.server.common.NationalConnectorGateway;
import eu.epsos.protocolterminators.ws.server.exception.NIException;
import eu.epsos.protocolterminators.ws.server.xca.DocumentSearchInterface;
import fi.kela.se.epsos.data.model.DocumentAssociation;
import fi.kela.se.epsos.data.model.DocumentFactory;
import fi.kela.se.epsos.data.model.EPDocumentMetaData;
import fi.kela.se.epsos.data.model.EPSOSDocument;
import fi.kela.se.epsos.data.model.MroDocumentMetaData;
import fi.kela.se.epsos.data.model.PSDocumentMetaData;
import fi.kela.se.epsos.data.model.SearchCriteria;
import fi.kela.se.epsos.data.model.SearchCriteria.Criteria;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Element;
import tr.com.srdc.epsos.securityman.exceptions.InsufficientRightsException;
import tr.com.srdc.epsos.util.XMLUtil;

/**
 * A national implementation of the document search interface.
 *
 * @author Kostas Karkaletsis, k.karkaletsis@gnomon.com.gr
 */
public class DocumentSearchImpl extends NationalConnectorGateway implements DocumentSearchInterface {

    // Logger
    private static final Logger logger = Logger.getLogger(DocumentSearchImpl.class);
    // Patient summary class code
    private static final String PS_CLASS_CODE = "60591-5";
    private static final String EP_CLASS_CODE = "57833-6";
    // Repository ID
    private static final String DEFAULT_REPOSITORY_ID = "1.3.182.2.3";

    /**
     * A method returning patient summary document meta data list both in XML
     * and PDF format matching the given search criteria.
     *
     * @param searchCriteria the search criteria of the document.
     * @return a list of meta data document associations.
     * @throws NIException, InsufficientRightsException
     */
    @Override
    public DocumentAssociation<PSDocumentMetaData> getPSDocumentList(SearchCriteria searchCriteria) throws NIException, InsufficientRightsException {
        // Extracting the patient id criteria
        String patientId = searchCriteria.getCriteriaValue(Criteria.PatientId);

        // TODO Implement SOA/WS based patient summary document list
        logger.info("PS Implement SOA/WS based patient summary document list for patient identified by the given id: '" + patientId + "'.");

        // Creating a patient summary document association metadata
        DocumentAssociation<PSDocumentMetaData> association = null;

        boolean documentFound = false;
        if (documentFound) {
            try {
                logger.info("Matching document list for patient identified by the given id: '" + patientId + "'.");

                // Fetch document id from your system
                String documentId = new String();
                documentId += ".PS.1";

                // Creating the patient summary XML document metadata
                // Fetch these values from your system
                Date documentDate = new Date();
                String repositoryId = new String();
                String documentTitle = new String();
                String documentAuthor = new String();

                PSDocumentMetaData xmlmd = DocumentFactory.createPSDocumentXML(documentId, patientId, documentDate, repositoryId, documentTitle, documentAuthor);

                // Creating the patient sumamry PDF document metadata
                String pdfid = documentId.substring(0, documentId.length() - 1) + '2';
                PSDocumentMetaData pdfmd = DocumentFactory.createPSDocumentPDF(pdfid, patientId, documentDate, repositoryId, documentTitle, documentAuthor);

                // Setting up a patient summary document association metadata
                association = DocumentFactory.createDocumentAssociation(xmlmd, pdfmd);
            } catch (Exception exc) {
                logger.error("An error occurred reading from the file for the patient identified by the given id: '" + patientId + "'.");
                exc.printStackTrace();
            }
        } else {
            logger.info("No matching document list found for the patient identified by the given id: '" + patientId + "'.");
        }

        return association;
    }

    /**
     * A method returning one document including document meta data and the DOM
     * document itself matching the search criteria.
     *
     * @param searchCriteria the search criteria of the document.
     * @return the document matched by the criteria.
     * @throws NIException, InsufficientRightsException
     */
    @Override
    public EPSOSDocument getDocument(SearchCriteria searchCriteria) throws NIException, InsufficientRightsException {
        String patientId = searchCriteria.getCriteriaValue(Criteria.PatientId);
        logger.info("TODO Implement SOA/WS based patient summary document both XML and PDF for patient identified by the given id: '" + patientId + "'.");
        String documentId = searchCriteria.getCriteriaValue(Criteria.DocumentId);

        EPSOSDocument document = null;

        // Matching to the requested ps document
        if (documentId.endsWith(".PS.1")) {
            try {
                logger.info("Matching EPSOS PS XML document identified by id '" + documentId + "'.");

                // Fetch or create here the cda xml file
                String xmlString = new String();
                document = DocumentFactory.createEPSOSDocument(patientId, PS_CLASS_CODE, XMLUtil.parseContent(xmlString));
            } catch (Exception exc) {
                logger.info("An error occurred parsing XML version document.");
                exc.printStackTrace();
            }
        } else if (documentId.endsWith(".PS.2")) {
            try {
                logger.info("Matching EPSOS PS PDF XML document identified by id '" + documentId + "'.");
                String xmlString = new String();
                document = DocumentFactory.createEPSOSDocument(patientId, PS_CLASS_CODE, XMLUtil.parseContent(xmlString));
            } catch (Exception exc) {
                logger.info("An error occurred parsing PDF XML version document.");
                exc.printStackTrace();
            }
        } else {
            logger.info("No matching document found for patient identified by id '" + patientId + "'.");
        }

        // Matching to the requested pe document
        if (documentId.endsWith(".EP.1")) {
            try {
                logger.info("Matching EPSOS EP XML document identified by id '" + documentId + "'.");

                String xmlString = new String();
                document = DocumentFactory.createEPSOSDocument(patientId, EP_CLASS_CODE, XMLUtil.parseContent(xmlString));
            } catch (Exception exc) {
                logger.info("An error occurred parsing XML version document.");
                exc.printStackTrace();
            }
        } else if (documentId.endsWith(".EP.2")) {
            try {
                logger.info("Matching EPSOS EP PDF XML document identified by id '" + documentId + "'.");

                String xmlString = new String();
                document = DocumentFactory.createEPSOSDocument(patientId, EP_CLASS_CODE, XMLUtil.parseContent(xmlString));
            } catch (Exception exc) {
                logger.info("An error occurred parsing PDF XML version document.");
                exc.printStackTrace();
            }
        } else {
            logger.info("No matching document found for patient identified by id '" + patientId + "'.");
        }

        return document;
    }

    @Override
    public DocumentAssociation<MroDocumentMetaData> getMroDocumentList(SearchCriteria searchCriteria) throws NIException, InsufficientRightsException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<DocumentAssociation<EPDocumentMetaData>> getEPDocumentList(SearchCriteria searchCriteria) throws NIException, InsufficientRightsException {

        List<DocumentAssociation<EPDocumentMetaData>> epList = new ArrayList();
        // Extracting the patient id criteria
        String patientId = searchCriteria.getCriteriaValue(Criteria.PatientId);

        // TODO Implement SOA/WS based patient summary document list
        logger.info("TODO Implement SOA/WS based patient summary document list for patient identified by the given id: '" + patientId + "'.");

        // Creating a patient summary document association metadata
        DocumentAssociation<EPDocumentMetaData> association = null;

        boolean documentFound = false;
        if (documentFound) {

            try {
                logger.info("Matching EP document list for patient identified by the given id: '" + patientId + "'.");

// Fetch document id from your system
                String documentId = new String();
                documentId += ".EP.1";

                // Creating the patient summary XML document metadata
                // Fetch these values from your system
                Date documentDate = new Date();
                String repositoryId = new String();
                String documentTitle = new String();
                String documentAuthor = new String();

                // Creating the patient summary XML document metadata
                EPDocumentMetaData xmlmd = DocumentFactory.createEPDocumentXML(documentId, patientId, documentDate, repositoryId, documentTitle, documentAuthor);

                // Creating the patient sumamry PDF document metadata
                String pdfid = documentId.substring(0, documentId.length() - 1) + '2';
                logger.info("EP PDF DOCUMENT ID: '" + pdfid);
                EPDocumentMetaData pdfmd = DocumentFactory.createEPDocumentPDF(pdfid, patientId, documentDate, repositoryId, documentTitle, documentAuthor);

                // Setting up a patient summary document association metadata
                association = DocumentFactory.createDocumentAssociation(xmlmd, pdfmd);
            } catch (Exception exc) {
                logger.error("An error occurred reading from the file for the patient identified by the given id: '" + patientId + "'.");
                exc.printStackTrace();
            }
        } else {
            logger.info("No matching document list found for the patient identified by the given id: '" + patientId + "'.");
        }
        epList.add(association);
        return epList;

    }

    // TMP
    private String print(String content) throws Exception {
        org.w3c.dom.Document d = XMLUtil.parseContent(content);
        OutputFormat format = new OutputFormat(d);
        format.setLineWidth(85);
        format.setIndenting(true);
        format.setIndent(2);
        Writer out = new StringWriter();
        XMLSerializer serializer = new XMLSerializer(out, format);
        serializer.serialize(d);

        return out.toString();
    }

    public void setSOAPHeader(Element elmnt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
