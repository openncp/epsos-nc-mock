package eu.epsos.protocolterminators.ws.server.xcpd.impl;

import eu.epsos.protocolterminators.ws.server.common.NationalConnectorGateway;
import eu.epsos.protocolterminators.ws.server.exception.NIException;
import eu.epsos.protocolterminators.ws.server.xcpd.PatientSearchInterface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import tr.com.srdc.epsos.data.model.PatientDemographics;
import tr.com.srdc.epsos.data.model.PatientId;
import tr.com.srdc.epsos.securityman.exceptions.InsufficientRightsException;

/**
 * A national implementation of the patient search interface.
 *
 * @author Kostas Karkaletsis, k.karkaletsis@gnomon.com.gr
 */
public class PatientSearchImpl extends NationalConnectorGateway implements PatientSearchInterface {

    // Logger
    private static final Logger logger = Logger.getLogger(PatientSearchImpl.class);

    /**
     * A method returning the demographics of a patient identified by the given
     * id list.
     *
     * @param ids the patient id list.
     * @return a list of patient demographics.
     * @throws NIException, InsufficientRightsException
     */
    @Override
    public List<PatientDemographics> getPatientDemographics(List<PatientId> idList) throws NIException, InsufficientRightsException {
        // Building the patient demographics
        List<PatientDemographics> patientDemographicslist = new ArrayList<PatientDemographics>(1);

        // Extracting patient identification criteria
        String patientId = idList.get(0).getExtension();

        // TODO Implement soa based patient search demographics instead
        logger.info("TODO Implement SOA/WS based patient search demographics for patient identified by the given id: '" + patientId + "'.");
        boolean patientFound = false;
        String city = "";

        if (patientFound) {
            try {
                logger.info("Matching patient identified by the given id: '" + patientId + "'.");

                // Setting up the demographic data, you have to replace the new String with the data come from your system
                PatientDemographics demographics = new PatientDemographics();
                demographics.setIdList(Arrays.asList(idList.get(0)));
                demographics.setFamilyName(new String());
                demographics.setGivenName(new String());
                demographics.setEmail(new String());
                demographics.setStreetAddress(new String());
                demographics.setPostalCode(new String());
                demographics.setTelephone(new String());
                demographics.setCity(new String());
                demographics.setCountry(new String());
                // Adding data into demographics list
                patientDemographicslist.add(demographics);
            } catch (Exception exc) {
                logger.error("An error occurred reading from the file for the patient identified by the given id: '" + patientId + "'.");
                exc.printStackTrace();
            }
        } else {
            logger.info("No matching found for the patient identified by the given id: '" + patientId + "'.");
        }

        return patientDemographicslist;
    }

    @Override
    public String getPatientId(String citizenNumber) throws NIException, InsufficientRightsException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
