package repconnector;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;


public class REPConnector {
	private static final String IP = "147.27.50.131";
	//Document types
	public enum DocType {PS_XML,PS_PDF,EP_XML,EP_PDF}
	
	public REPConnector() {
	}

	//Get a string version of the document type
	public String getDocTypeStr(DocType docType)
	{
		switch(docType)
     	{
     		case PS_XML:
     			return "PS_XML";
     		case PS_PDF:
     			return "PS_PDF";
     		case EP_XML:
     			return "EP_XML";
     		case EP_PDF:
     			return "EP_PDF";
     		default:
     			return null;	
     	}
	}
	
	public String getDocument(String patientid,DocType psPdf, Cookie cookie)
	{
		//Prepare the URL for the request
		String urlString = "http://" + IP + ":8080/EPSOS_REP/SelectDocument";
		URL url;
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e) {
			return null;
		}
		HttpURLConnection connection;
		try {
			connection = (HttpURLConnection)url.openConnection();
		} catch (IOException e) {

			return null;
		}
		
		//set the parameters
		String urlParameters = "<request> <patientId>" + patientid + "</patientId> <docType>" + getDocTypeStr(psPdf) + "</docType> </request>";
		try {
			urlParameters = "data=" + URLEncoder.encode(urlParameters, "UTF-8");
		} catch (UnsupportedEncodingException e) {

			return null;
		}
		//set the request method
		connection.setDoOutput(true);
		try {
			connection.setRequestMethod("POST");
		} catch (ProtocolException e) {

			return null;
		}
		
		//set cookie
		connection.setRequestProperty("Cookie", cookie.getName()+"="+cookie.getValue());
		//set request header
		connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
		
		//send post request
		DataOutputStream wr = null;
		try{
			wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
		}catch(Exception e)
		{

			return null;
		}

		
		String response = null;
		
		//check status code
		try {
			if(connection.getResponseCode() == 200){		
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				//get response
			    if(reader != null){
			    	StringBuffer sb = new StringBuffer();
			    	String line = new String();
			    	while ((line = reader.readLine()) != null)
						sb.append(line);
			    	reader.close();
			    	response = sb.toString();
			    }		     
			}
		} catch (IOException e) {

			return null;
		}	
		
		return response;
	}
	
	
}
